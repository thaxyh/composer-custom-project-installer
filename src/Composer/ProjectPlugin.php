<?php
namespace ThaXyh\Composer\Installer;

use Composer\Plugin\PluginInterface;
use Composer\Composer;
use Composer\IO\IOInterface;
use Composer\EventDispatcher\EventSubscriberInterface;
use Composer\Script\ScriptEvents;
use Composer\Installer\PackageEvent;
use Composer\Package\PackageInterface;
use Composer\DependencyResolver\Operation\InstallOperation;
use Composer\DependencyResolver\Operation\UpdateOperation;

class ProjectPlugin implements PluginInterface, EventSubscriberInterface
{

    /**
     * @var ProjectInstaller
     */
    private $installer;

    /**
     * @var IOInterface
     */
    private $io;

    /**
     * {@inheritdoc}
     */
    public function activate(Composer $composer, IOInterface $io)
    {
        $this->io        = $io;
        $this->installer = new ProjectInstaller($io, $composer, 'project');
        $composer->getInstallationManager()->addInstaller($this->installer);
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents(): array
    {
        return [
            ScriptEvents::POST_PACKAGE_INSTALL => 'postPackage',
            ScriptEvents::POST_PACKAGE_UPDATE  => 'postPackage',
        ];
    }

    /**
     * Executes post-package event action.
     * @param PackageEvent $event
     */
    public function postPackage(PackageEvent $event)
    {
        $package = $this->getPackageFromEvent($event);

        if (!$package) {
            return;
        }

        if ($this->installer->hasAlteredDestinationPath($package)) {
            $destinationPath = $this->installer->getInstallPath($package);

            $this->io->write("<info>Package <options=bold>{$package->getPrettyName()}</> has got an altered destination path <options=bold>{$destinationPath}</>.</info>");
        }
    }

    /**
     * Retrieves relevant package from the event.
     * @param PackageEvent $event
     * @return PackageInterface
     * @throws \Exception
     */
    protected function getPackageFromEvent(PackageEvent $event)
    {
        $operation = $event->getOperation();

        if ($operation instanceof InstallOperation) {
            return $operation->getPackage();
        } elseif ($operation instanceof UpdateOperation) {
            return $operation->getTargetPackage();
        }

        return;
    }

}
