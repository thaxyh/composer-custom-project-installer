<?php
namespace ThaXyh\Composer\Installer;

use Composer\Installer\LibraryInstaller;
use Composer\Package\PackageInterface;

class ProjectInstaller extends LibraryInstaller
{

    /**
     * Helper flag for the purpose of indication of package's path alteration.
     * @var bool[]
     */
    private $alteredDestinationPath = [];

    /**
     * {@inheritDoc}
     */
    public function getInstallPath(PackageInterface $package)
    {
        foreach ($this->getExtraInstallerPaths() as $destinationPath => $packagePrettyName) {
            if (file_exists($destinationPath)) {
                if (!is_dir($destinationPath) || !is_writable($destinationPath)) {
                    throw new \RuntimeException("Installer path '{$destinationPath}' must be a writable directory.");
                }
            } else {
                @mkdir($destinationPath, 0775);
            }

            if (!is_array($packagePrettyName)) {
                $packagePrettyName = [ $packagePrettyName ];
            }

            if (in_array($package->getPrettyName(), $packagePrettyName)) {
                $this->alteredDestinationPath[$package->getPrettyName()] = true;

                return realpath($destinationPath);
            }
        }

        return parent::getInstallPath($package);
    }

    /**
     * Returns an array of extra.installer-paths paths or an empty
     * array if none found.
     * @return array
     */
    private function getExtraInstallerPaths(): array
    {
        $extra = $this->composer->getPackage()->getExtra();

        return filter_var(
                $extra['installer-paths'] ?? [],
                FILTER_DEFAULT,
                [
                    'flags' => FILTER_FORCE_ARRAY,
                ]
        );
    }

    /**
     * Returns true if a package's destination path was altered by user
     * via composer.json file.
     * @return bool
     */
    public function hasAlteredDestinationPath(PackageInterface $package): bool
    {
        return array_key_exists($package->getPrettyName(), $this->alteredDestinationPath)
                ? $this->alteredDestinationPath[$package->getPrettyName()]
                : false;
    }

}
